# README #

### Logic Calculator ###

* [App Description Needed]

### How do I get set up? ###

* Install SourceTree
* Install Xcode
* Click the download button and select "Clone in Source Tree" 
* Follow the prompts and select the local location for development.
* Click Source Control -> Pull in Xcode
* Open LogicCalculator.xcodeproj
* Write code
* Click Source Control -> Commit in Xcode
* Click Source Control -> Push in Xcode 

If you have any questions, contact Reid. 

### Have problems? Stuck on a bug? ###

Contact your teammates.  Coding together is more fun and more productive, so ask for help. 


### Resources ###

* Google Document: https://docs.google.com/document/d/1OL9unV146ojTIibimYYAZCrSchuk8EJhLgJcyvHX0_M/edit
* Project Proposal: https://docs.google.com/document/d/1tbl55tFzCrZiMGzYjtMqzraMpX6KF57Pd61NxDIG2-s/edit