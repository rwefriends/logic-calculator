//
//  TruthTableTests.swift
//  Logic Calculator
//
//  Created by Reid Long on 5/17/15.
//  Copyright (c) 2015 RWE Friends. All rights reserved.
//

import XCTest

class TruthTableTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testMatrix() {
        let truthTable = TruthTable(inputs: 3, outputs: 1)
        let matrix = truthTable.matrix
        self.doesCountInBinary(matrix, rows: matrix.count, columns: truthTable.numberOfInputs)
        //Spot checking for extra validation
        XCTAssert(matrix[0][0] == false)
        XCTAssert(matrix[0][2] == false)
        XCTAssert(matrix[3][1] == true)
        XCTAssert(matrix[3][2] == true)
        XCTAssert(matrix[5][0] == true)
        XCTAssert(matrix[5][2] == true)
        XCTAssert(matrix[7][0] == true)
        XCTAssert(matrix[7][1] == true)
        XCTAssert(matrix[7][2] == true)
        
    }
    
    func doesCountInBinary(matrix: [[Bool]], rows: Int, columns: Int) {
        
        for index in 0..<rows {
            var rowSum = 0
            for subIndex in 0..<columns {
                let calibratedValue = 1 << (columns - subIndex - 1)
                rowSum += matrix[index][subIndex] ? calibratedValue : 0
            }
            XCTAssert(rowSum == index, "Row Not Counting In Binary \(rowSum) not \(index)")
        }
        XCTAssert(true, "All tests past")
    }

}
