//
//  BooleanExpression.swift
//  Logic Calculator
//
//  Created by Reid Long on 5/17/15.
//  Copyright (c) 2015 RWE Friends. All rights reserved.
//

import Foundation

enum BooleanExpressionType {
    case Invalid, POS, SOP, Simplfied, Normal
}

class BooleanExpression: Model {
    
    // Write code for the boolean expressions 
    
    var type: BooleanExpressionType = .Invalid
    
    // How should an expression be represented? This will probably depend on how the calculator/converters are implemented.
    
    override var description: String {
        assert(false)
        return "F = AB+C" // Don't leave this hardcoded!
    }
}