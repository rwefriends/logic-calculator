//
//  TruthTable.swift
//  Logic Calculator
//
//  Created by Reid Long on 5/17/15.
//  Copyright (c) 2015 RWE Friends. All rights reserved.
//

import Foundation


class TruthTable: Model {
    
    let tableSize: Int

    override init(inputs: Int, outputs: Int) {
        
        assert(outputs == 1, "Not implemented for multiple outputs")
        let rows = 1 << inputs // 2^inputs
        let columns = inputs + outputs
        self.tableSize = rows;
        self.matrix = Array(count: rows, repeatedValue: Array(count: columns, repeatedValue: false))
        super.init(inputs: inputs, outputs: outputs)
        self.createMatrix(rows, columns: columns)
    }
    
    private func createMatrix(rows: Int, columns: Int) {
        
        for rowIndex in 0..<rows {
            for columnIndex in 0..<self.numberOfInputs {
                
                self.matrix[rowIndex][self.numberOfInputs - columnIndex - 1] = rowIndex & (1 << columnIndex) != 0
                
            }
        }
        
    }
    
    var matrix: [[Bool]];
    
    var kmap: [[Bool]] {
        return self.createKmap()
    }
    
    // TODO: This could probably be refactored as a static utility function
    private func createKmap() -> [[Bool]]{
        assert(self.numberOfInputs <= 3, "Not Implemented for more than 3 inputs")
        // Forced to 2 Rows
        let kmapRows = 2
        // Required area is 2^inputs = self.tableSize
        let area = self.tableSize
        // Columns is then derrived
        let kmapColumns = area / kmapRows // This should always evaluate to a whole number
        var kmapMatrix = Array(count: kmapRows, repeatedValue: Array(count: kmapColumns, repeatedValue: false))
        
        let outputIndex = self.numberOfInputs // This will be used to determine the index in the truth table matrix of the output column
        
        for row in 0..<kmapRows {
            for column in 0..<kmapColumns {
                let index = row * kmapRows + column + TruthTable.kmapOffset(column, width: kmapColumns)
                kmapMatrix[row][column] = self.matrix[index][outputIndex]
            }
        }
        return kmapMatrix
        
    }
    
    // This is heavily hardcoded.  If somebody can come up with a more elegant solution
    // for generating kmaps, that would be much appreciated
    private class func kmapOffset(column: Int, width: Int) -> Int {
        let test = column % width
        switch(test) {
        case 2:
            return 1
        case 3:
            return -1
        default:
            return 0
        }
    }
}