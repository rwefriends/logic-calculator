//
//  Model.swift
//  Logic Calculator
//
//  Created by Reid Long on 5/17/15.
//  Copyright (c) 2015 RWE Friends. All rights reserved.
//

import Foundation

class Model : Printable, DebugPrintable {
    
    //Expand with additional fields that are needed for all of the models
    
    var numberOfInputs: Int
    var numberOfOutputs: Int
    
    init(inputs: Int, outputs: Int) {
        self.numberOfInputs = inputs
        self.numberOfOutputs = outputs
    }
    
    var description: String {
        assert(false)
        return "Model Description" //TODO: This should be improved
    }
    
    var debugDescription: String {
        return self.description
    }
}