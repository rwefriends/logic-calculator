//
//  TruthTable+POS.swift
//  Logic Calculator
//
//  Created by Reid Long on 5/18/15.
//  Copyright (c) 2015 RWE Friends. All rights reserved.
//

import Foundation

/*
* This extension is for the conversion from a TruthTable to a POS Boolean Expression.
* All helper functions and additional data structures should (hopefully) be encapsulated in this file.
*/
extension TruthTable {
    
    func toPOS() -> BooleanExpression {
        
        assert(false); //TODO: Implement this function
        return BooleanExpression(inputs: self.numberOfInputs, outputs: self.numberOfOutputs)
    }
}