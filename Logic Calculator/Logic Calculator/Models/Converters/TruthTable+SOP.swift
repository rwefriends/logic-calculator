//
//  TruthTable+SOP.swift
//  Logic Calculator
//
//  Created by Reid Long on 5/18/15.
//  Copyright (c) 2015 RWE Friends. All rights reserved.
//

import Foundation


/*
* This extension is for the conversion from TruthTables to SOP Boolean Expressions.
* All helper functions and additional data structures should (hopefully) be encapsulated in this file.
*/
extension TruthTable {
    
    func toSOP() -> BooleanExpression {
        
        assert(false); //TODO: Implement this method
        return BooleanExpression(inputs: self.numberOfInputs, outputs: self.numberOfOutputs)
    }
}