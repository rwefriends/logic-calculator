//
//  BooleanExpression+TruthTable.swift
//  Logic Calculator
//
//  Created by Reid Long on 5/18/15.
//  Copyright (c) 2015 RWE Friends. All rights reserved.
//

import Foundation


/*
 * This extension is for the conversion from BooleanExpressions to TruthTables. 
 * All helper functions and additional data structures should (hopefully) be encapsulated in this file.
 */
extension BooleanExpression {
    
    func toTruthTable() -> TruthTable {
        assert(false); //TODO: Implement this function
        return TruthTable(inputs: self.numberOfInputs, outputs: self.numberOfOutputs)
    }
}