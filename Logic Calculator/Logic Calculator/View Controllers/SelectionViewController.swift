//
//  SelectionViewController.swift
//  Logic Calculator
//
//  Created by Reid Long on 5/17/15.
//  Copyright (c) 2015 RWE Friends. All rights reserved.
//

import UIKit

/*
 * Superclass ViewController for handling selection events. This may or may not
 * be needed depending on what type of generality the selection view controllers have. 
 * InputSelectionViewController and OutputSelectionViewController inherit from this class
 */
class SelectionViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}